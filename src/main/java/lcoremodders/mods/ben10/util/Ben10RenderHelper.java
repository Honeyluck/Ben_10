package lcoremodders.mods.ben10.util;

import lcoremodders.mods.ben10.Ben10;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.Vec3d;

import java.awt.*;

public class Ben10RenderHelper {

	public static ResourceLocation iconTex = new ResourceLocation(Ben10.MODID, "textures/gui/ability_icons.png");

	public static void drawIcon(Minecraft mc, Gui gui, int x, int y, int row, int column) {
		mc.renderEngine.bindTexture(iconTex);
		gui.drawTexturedModalRect(x, y, column * 16, row * 16, 16, 16);
	}

	public static void drawSuperpowerIcon(Minecraft mc, Gui gui, int x, int y, int row, int col) {
		mc.getTextureManager().bindTexture(new ResourceLocation(Ben10.MODID, "textures/gui/superpower_icons.png"));
		gui.drawTexturedModalRect(x, y, col * 32, row * 32, 32, 32);
	}

	public static void drawCuboid(Vec3d start, Vec3d end, float thickness, Color outerColor, Color innerColor) {
		if (start != null && end != null) {
			Tessellator tessellator = Tessellator.getInstance();
			BufferBuilder bb = tessellator.getBuffer();
			int layers = 2;
			GlStateManager.pushMatrix();
			start = start.scale(-1.0D);
			end = end.scale(-1.0D);
			GlStateManager.translate(-start.x, -start.y, -start.z);
			start = end.subtract(start);
			end = end.subtract(end);
			double x = end.x - start.x;
			double y = end.y - start.y;
			double z = end.z - start.z;
			double diff = MathHelper.sqrt(x * x + z * z);
			float yaw = (float)(Math.atan2(z, x) * 180.0D / 3.141592653589793D) - 90.0F;
			float pitch = (float)(-(Math.atan2(y, diff) * 180.0D / 3.141592653589793D));
			GlStateManager.rotate(-yaw, 0.0F, 1.0F, 0.0F);
			GlStateManager.rotate(pitch, 1.0F, 0.0F, 0.0F);

			for(int layer = 0; layer <= layers; ++layer) {
				if (layer < layers) {
					Color color = layer == 0 ? outerColor : innerColor;
					GlStateManager.color((float)color.getRed() / 255.0F, (float)color.getGreen() / 255.0F, (float)color.getBlue() / 255.0F, 1.0F / (float)layers / 2.0F);
					GlStateManager.depthMask(false);
				} else {
					GlStateManager.color(1.0F, 1.0F, 1.0F, 1);
					GlStateManager.depthMask(true);
				}

				double size = (double)thickness + (layer < layers ? 1 - (layer * thickness) : 0.0D);
				double d = size * 0.0625;
				double width = 0.0625D * size;
				double height = 0.0625D * size;
				double length = start.distanceTo(end) + d;
				bb.begin(7, DefaultVertexFormats.POSITION);
				bb.pos(-width, height, length).endVertex();
				bb.pos(width, height, length).endVertex();
				bb.pos(width, height, -d).endVertex();
				bb.pos(-width, height, -d).endVertex();
				bb.pos(width, -height, -d).endVertex();
				bb.pos(width, -height, length).endVertex();
				bb.pos(-width, -height, length).endVertex();
				bb.pos(-width, -height, -d).endVertex();
				bb.pos(-width, -height, -d).endVertex();
				bb.pos(-width, -height, length).endVertex();
				bb.pos(-width, height, length).endVertex();
				bb.pos(-width, height, -d).endVertex();
				bb.pos(width, height, length).endVertex();
				bb.pos(width, -height, length).endVertex();
				bb.pos(width, -height, -d).endVertex();
				bb.pos(width, height, -d).endVertex();
				bb.pos(width, -height, length).endVertex();
				bb.pos(width, height, length).endVertex();
				bb.pos(-width, height, length).endVertex();
				bb.pos(-width, -height, length).endVertex();
				bb.pos(width, -height, -d).endVertex();
				bb.pos(width, height, -d).endVertex();
				bb.pos(-width, height, -d).endVertex();
				bb.pos(-width, -height, -d).endVertex();
				tessellator.draw();
			}

			GlStateManager.popMatrix();
		}
	}
}