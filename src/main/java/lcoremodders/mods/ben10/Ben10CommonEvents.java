package lcoremodders.mods.ben10;

import lcoremodders.mods.ben10.abilities.AbilitySuperJump;
import lcoremodders.mods.ben10.items.ItemOGOmnitrix;
import lcoremodders.mods.ben10.superpowers.Ben10Superpowers;
import lucraft.mods.lucraftcore.extendedinventory.InventoryExtendedInventory;
import lucraft.mods.lucraftcore.extendedinventory.capabilities.CapabilityExtendedInventory;
import lucraft.mods.lucraftcore.superpowers.SuperpowerHandler;
import lucraft.mods.lucraftcore.superpowers.abilities.Ability;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.event.entity.living.LivingEvent;
import net.minecraftforge.event.entity.living.LivingFallEvent;
import net.minecraftforge.event.entity.player.PlayerFlyableFallEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;

public class Ben10CommonEvents {

	@SubscribeEvent
	public void playerTick(TickEvent.PlayerTickEvent e) {
		EntityPlayer player = e.player;
		if (player.getCapability(CapabilityExtendedInventory.EXTENDED_INVENTORY_CAP, null).getInventory().getStackInSlot(InventoryExtendedInventory.SLOT_WRIST).isEmpty()) {
			if (SuperpowerHandler.hasSuperpower(player, Ben10Superpowers.omnitrix)) {
				SuperpowerHandler.removeSuperpower(player);
			}
		} else {
			if (!(player.getCapability(CapabilityExtendedInventory.EXTENDED_INVENTORY_CAP, null).getInventory().getStackInSlot(InventoryExtendedInventory.SLOT_WRIST).getItem() instanceof ItemOGOmnitrix)) {
				if (SuperpowerHandler.hasSuperpower(player, Ben10Superpowers.omnitrix)) {
					SuperpowerHandler.removeSuperpower(player);
				}
			}
		}
	}

	@SubscribeEvent
	public void livingJump(LivingEvent.LivingJumpEvent e) {
		if (e.getEntityLiving() instanceof EntityPlayer) {
			EntityPlayer player = (EntityPlayer) e.getEntityLiving();
			for (AbilitySuperJump ab : Ability.getAbilitiesFromClass(Ability.getAbilities(player), AbilitySuperJump.class)) {
				if (ab != null && ab.getDataManager().get(Ability.SHOW_IN_BAR)) {
					if(ab.getCharge() > 0 && e.getEntityLiving().onGround) {
						e.getEntityLiving().motionY = -1;
						ab.jump();
					}
				}
			}
		}
	}

	@SubscribeEvent
	public void onLivingFall(LivingFallEvent e) {
		if(e.getEntityLiving() instanceof EntityPlayer) {
			if (e.getEntityLiving().isInWater() || e.getEntityLiving().onGround)
				superJumpLanding((EntityPlayer) e.getEntityLiving());
		}
	}

	@SubscribeEvent
	public void onPlayerFlyableFall(PlayerFlyableFallEvent e) {
		if (e.getEntityPlayer().isInWater() || e.getEntityPlayer().onGround)
			superJumpLanding(e.getEntityPlayer());
	}

	public void superJumpLanding(EntityPlayer player) {
		for (AbilitySuperJump ab : Ability.getAbilitiesFromClass(Ability.getAbilities(player), AbilitySuperJump.class)) {
			if (ab != null && ab.getDataManager().get(Ability.SHOW_IN_BAR)) {
				if (!ab.hasLanded() && ab.getCharge() > 0) {
					if (player.isSneaking()) {
						player.getEntityWorld().newExplosion(player, player.posX, player.posY, player.posZ, player.fallDistance / 20F, false, true);
					}

					ab.setCharge(0);
					ab.setHasLanded(true);
					System.out.println("Hi");
				}
			}
		}
	}
}