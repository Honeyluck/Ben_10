package lcoremodders.mods.ben10.items;

import lcoremodders.mods.ben10.Ben10;
import lcoremodders.mods.ben10.items.ItemOGOmnitrix;
import lucraft.mods.lucraftcore.util.helper.ItemHelper;
import net.minecraft.item.Item;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@Mod.EventBusSubscriber
public class Ben10Items {

	public static ItemOGOmnitrix OMNITRIX_OG = new ItemOGOmnitrix();

	@SubscribeEvent
	public static void registerItems(RegistryEvent.Register<Item> e) {
		e.getRegistry().registerAll(
				OMNITRIX_OG
		);
	}

	@SubscribeEvent
	@SideOnly(Side.CLIENT)
	public static void registerModels(ModelRegistryEvent e) {
		ItemHelper.registerItemModel(OMNITRIX_OG, Ben10.MODID,"og_omnitrix");
	}
}