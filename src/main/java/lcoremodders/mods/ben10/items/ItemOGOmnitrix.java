package lcoremodders.mods.ben10.items;

import lcoremodders.mods.ben10.superpowers.Ben10Superpowers;
import lucraft.mods.lucraftcore.extendedinventory.IItemExtendedInventory;
import lucraft.mods.lucraftcore.superpowers.SuperpowerHandler;
import lucraft.mods.lucraftcore.util.items.ItemBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumHand;
import net.minecraft.world.World;

public class ItemOGOmnitrix extends ItemBase implements IItemExtendedInventory {

	public ItemOGOmnitrix() {
		super("og_omnitrix");
		this.maxStackSize = 1;
	}

	@Override
	public ActionResult<ItemStack> onItemRightClick(World worldIn, EntityPlayer playerIn, EnumHand handIn) {
		return super.onItemRightClick(worldIn, playerIn, handIn);
	}

	@Override
	public ExtendedInventoryItemType getEIItemType(ItemStack stack) {
		return ExtendedInventoryItemType.WRIST;
	}

	@Override
	public void onEquipped(ItemStack itemstack, EntityPlayer player) {
		if (!itemstack.isEmpty()) {
			SuperpowerHandler.setSuperpower(player, Ben10Superpowers.omnitrix);
		}
	}

	@Override
	public void onUnequipped(ItemStack itemstack, EntityPlayer player) {
		SuperpowerHandler.removeSuperpower(player);
	}
}