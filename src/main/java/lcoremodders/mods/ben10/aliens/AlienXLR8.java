package lcoremodders.mods.ben10.aliens;

import lcoremodders.mods.ben10.Ben10;
import lcoremodders.mods.ben10.abilities.*;
import lcoremodders.mods.ben10.abilities.conditions.AbilityConditionAlien;
import lcoremodders.mods.ben10.abilities.conditions.AbilityConditionIsTransformed;
import lucraft.mods.lucraftcore.superpowers.abilities.Ability;
import lucraft.mods.lucraftcore.superpowers.abilities.predicates.AbilityConditionAbility;
import net.minecraft.entity.EntityLivingBase;

import java.util.UUID;

public class AlienXLR8 extends Alien {

	public AlienXLR8() {
		super("xlr8");
		setRegistryName(Ben10.MODID, "xlr8");
	}

	private UUID uuid = UUID.fromString("1fd54d4e-3190-48d8-ac18-d5b7e78707c9");

	@Override
	public Ability.AbilityMap addDefaultAbilities(EntityLivingBase entity, Ability.AbilityMap abilities, Ability.EnumAbilityContext context, AbilityOmnitrix abilityOmnitrix) {
		AbilitySuperSpeed abilitySuperSpeed = new AbilitySuperSpeed(entity);

		abilities.put("xlr8_tornado", new AbilityTornado(entity).addCondition(new AbilityConditionIsTransformed(abilityOmnitrix)).addCondition(new AbilityConditionAlien(this)));
		abilities.put("xlr8_super_speed", abilitySuperSpeed.addCondition(new AbilityConditionIsTransformed(abilityOmnitrix)).addCondition(new AbilityConditionAlien(this)));

		abilities.put("xlr8_increase_speed", new AbilityChangeSpeed(entity).addCondition(new AbilityConditionAbility(abilitySuperSpeed)));
		abilities.put("xlr8_decrease_speed", new AbilityChangeSpeed(entity).setDataValue(AbilityChangeSpeed.INCREASE_SPEED, false).addCondition(new AbilityConditionAbility(abilitySuperSpeed)));
		abilities.put("xlr8_wall_running", new AbilityWallRunning(entity).addCondition(new AbilityConditionAbility(abilitySuperSpeed)));


		return abilities;
	}
}