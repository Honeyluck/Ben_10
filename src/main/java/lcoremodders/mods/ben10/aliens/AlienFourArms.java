package lcoremodders.mods.ben10.aliens;

import lcoremodders.mods.ben10.Ben10;
import lcoremodders.mods.ben10.abilities.AbilityOmnitrix;
import lcoremodders.mods.ben10.abilities.AbilitySuperJump;
import lcoremodders.mods.ben10.abilities.conditions.AbilityConditionAlien;
import lcoremodders.mods.ben10.abilities.conditions.AbilityConditionIsTransformed;
import lucraft.mods.lucraftcore.superpowers.abilities.*;
import net.minecraft.entity.EntityLivingBase;

import java.util.UUID;

public class AlienFourArms extends Alien {

	public AlienFourArms() {
		super("four_arms");
		setRegistryName(Ben10.MODID, "four_arms");
	}

	public UUID uuid = UUID.fromString("4bc68137-8355-4bf1-bc4f-d56173b259b2");

	@Override
	public Ability.AbilityMap addDefaultAbilities(EntityLivingBase entity, Ability.AbilityMap abilities, Ability.EnumAbilityContext context, AbilityOmnitrix abilityOmnitrix) {
		abilities.put("four_arms_jump_boost", new AbilityJumpBoost(entity).setDataValue(AbilityAttributeModifier.UUID, uuid).setDataValue(AbilityAttributeModifier.AMOUNT, 3F) .setDataValue(AbilityAttributeModifier.OPERATION, 1).addCondition(new AbilityConditionIsTransformed(abilityOmnitrix)).addCondition(new AbilityConditionAlien(this)));
		abilities.put("four_arms_strength", new AbilityStrength(entity).setDataValue(AbilityAttributeModifier.UUID, uuid).setDataValue(AbilityAttributeModifier.AMOUNT, 1F) .addCondition(new AbilityConditionIsTransformed(abilityOmnitrix)).addCondition(new AbilityConditionAlien(this)));
		abilities.put("four_arms_punch", new AbilityPunch(entity).setDataValue(AbilityAttributeModifier.UUID, uuid).setDataValue(AbilityAttributeModifier.AMOUNT, 2F) .addCondition(new AbilityConditionIsTransformed(abilityOmnitrix)).addCondition(new AbilityConditionAlien(this)));
		abilities.put("four_arms_fall_resistance", new AbilityFallResistance(entity).setDataValue(AbilityAttributeModifier.UUID, uuid).setDataValue(AbilityAttributeModifier.AMOUNT, 0.05f).setDataValue(AbilityAttributeModifier.OPERATION, 1).addCondition(new AbilityConditionIsTransformed(abilityOmnitrix)).addCondition(new AbilityConditionAlien(this)));

		abilities.put("four_arms_health", new AbilityHealth(entity).setDataValue(AbilityAttributeModifier.UUID, uuid).setDataValue(AbilityAttributeModifier.AMOUNT, 5F) .addCondition(new AbilityConditionIsTransformed(abilityOmnitrix)).addCondition(new AbilityConditionAlien(this)));
		abilities.put("four_arms_super_jump", new AbilitySuperJump(entity).addCondition(new AbilityConditionIsTransformed(abilityOmnitrix)).addCondition(new AbilityConditionAlien(this)));

		return abilities;
	}
}