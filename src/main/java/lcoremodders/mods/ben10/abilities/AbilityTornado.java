package lcoremodders.mods.ben10.abilities;

import lucraft.mods.lucraftcore.superpowers.abilities.AbilityHeld;
import lucraft.mods.lucraftcore.superpowers.abilities.data.AbilityData;
import lucraft.mods.lucraftcore.superpowers.abilities.data.AbilityDataInteger;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.network.play.server.SPacketEntityVelocity;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;

import java.util.List;

public class AbilityTornado extends AbilityHeld {

	public static final AbilityData<Integer> DURATION = new AbilityDataInteger("duration").disableSaving().enableSetting("duration", "Sets the duration, in seconds, you can make a tornado");
	public static final AbilityData<Integer> CAPTURE_RADIUS = new AbilityDataInteger("capture_radius").disableSaving().enableSetting("radius", "Sets the reach of the tornado to pull entities towards the normal radius");
	public static final AbilityData<Integer> RADIUS = new AbilityDataInteger("radius").disableSaving().enableSetting("radius", "Sets radius of the tornado, unit in blocks");
	public static final AbilityData<Integer> HEIGHT = new AbilityDataInteger("height").disableSaving().enableSetting("height", "How high an entity will be above the player");

	public AbilityTornado(EntityLivingBase entity) {
		super(entity);
	}

	@Override
	public void registerData() {
		super.registerData();
		this.dataManager.register(DURATION, 5);
		this.dataManager.register(RADIUS, 7);
		this.dataManager.register(CAPTURE_RADIUS, 15);
		this.dataManager.register(HEIGHT, 3);
	}

	@Override
	public void updateTick() {
		int radius = this.dataManager.get(RADIUS);
		int captureRadius = this.dataManager.get(CAPTURE_RADIUS);
		BlockPos ownerPos = entity.getPosition();

		double minimumY = ownerPos.getY() + entity.getEyeHeight() + this.dataManager.get(HEIGHT); // how high an entity must be from the player

		List<Entity> entitiesInArea = entity.world.getEntitiesWithinAABBExcludingEntity(entity, new AxisAlignedBB(ownerPos.add(-captureRadius, -captureRadius, -captureRadius), ownerPos.add(captureRadius, captureRadius, captureRadius)));
		for (Entity victimEntity : entitiesInArea) {
			{
				System.out.println("tornado pulling entity");
				if (victimEntity.getDistance(entity) < radius) { // move away from owner
					victimEntity.motionX += (victimEntity.posX - ownerPos.getX()) / 20;
					victimEntity.motionZ += (victimEntity.posZ - ownerPos.getZ()) / 20;
				} else if (victimEntity.getDistance(entity) > radius) { // move towards owner
					victimEntity.motionX += (ownerPos.getX() - victimEntity.posX) / 20;
					victimEntity.motionZ += (ownerPos.getZ() - victimEntity.posZ) / 20;
				}
				if (victimEntity.posY < minimumY || victimEntity.posY > minimumY) {
					System.out.println("change posy " + minimumY);
					victimEntity.motionY += (minimumY - victimEntity.posY) / 20;
				}
				if (victimEntity instanceof EntityPlayerMP) {
					((EntityPlayerMP) victimEntity).connection.sendPacket(new SPacketEntityVelocity(victimEntity));
				}
			}
		}
	}
}
