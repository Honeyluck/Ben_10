package lcoremodders.mods.ben10.abilities;

import lcoremodders.mods.ben10.util.Ben10RenderHelper;
import lucraft.mods.lucraftcore.superpowers.abilities.Ability;
import lucraft.mods.lucraftcore.superpowers.abilities.AbilityAction;
import lucraft.mods.lucraftcore.superpowers.abilities.data.AbilityData;
import lucraft.mods.lucraftcore.superpowers.abilities.data.AbilityDataBoolean;
import lucraft.mods.lucraftcore.superpowers.abilities.supplier.EnumSync;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Gui;
import net.minecraft.entity.EntityLivingBase;

public class AbilityChangeSpeed extends AbilityAction {

	public static final AbilityData<Boolean> INCREASE_SPEED = new AbilityDataBoolean("increase_speed").disableSaving().setSyncType(EnumSync.SELF).enableSetting("increase_speed", "Whether this ability increases or decreases speed.");

	public AbilityChangeSpeed(EntityLivingBase entity) {
		super(entity);
	}

	@Override
	public void registerData() {
		super.registerData();
		this.dataManager.register(INCREASE_SPEED, true);
	}

	@Override
	public void drawIcon(Minecraft mc, Gui gui, int x, int y) {
		Ben10RenderHelper.drawIcon(mc, gui, x, y, 0, getDataManager().get(INCREASE_SPEED) ? 8 : 7);
	}

	@Override
	public String getTranslationDescription() {
		return getModId() + ".abilities." + getUnlocalizedName() + (getDataManager().get(INCREASE_SPEED) ? ".increase" : ".decrease") + ".desc";
	}

	@Override
	public boolean action() {
		for (AbilitySuperSpeed ab : Ability.getAbilitiesFromClass(Ability.getAbilities(entity), AbilitySuperSpeed.class)) {
			ab.setSpeedLevel(ab.getSpeedLevel() + (this.getDataManager().get(INCREASE_SPEED) ? 1 : -1));
			ab.updateAttribute();
		}
		return true;
	}
}
