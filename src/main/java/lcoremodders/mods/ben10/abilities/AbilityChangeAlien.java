package lcoremodders.mods.ben10.abilities;

import lcoremodders.mods.ben10.util.Ben10RenderHelper;
import lucraft.mods.lucraftcore.superpowers.abilities.Ability;
import lucraft.mods.lucraftcore.superpowers.abilities.AbilityAction;
import lucraft.mods.lucraftcore.superpowers.abilities.data.AbilityData;
import lucraft.mods.lucraftcore.superpowers.abilities.data.AbilityDataBoolean;
import lucraft.mods.lucraftcore.superpowers.abilities.supplier.EnumSync;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Gui;
import net.minecraft.entity.EntityLivingBase;

public class AbilityChangeAlien extends AbilityAction {

	public static final AbilityData<Boolean> NEXT_ALIEN = new AbilityDataBoolean("next_alien").disableSaving().setSyncType(EnumSync.SELF).enableSetting("next_alien", "Whether this ability changes to the next or previous alien.");

	public AbilityChangeAlien(EntityLivingBase entity) {
		super(entity);
	}

	@Override
	public void registerData() {
		super.registerData();
		this.dataManager.register(NEXT_ALIEN, true);
	}

	@Override
	public void drawIcon(Minecraft mc, Gui gui, int x, int y) {
		Ben10RenderHelper.drawIcon(mc, gui, x, y, 0, getDataManager().get(NEXT_ALIEN) ? 1 : 0);
	}

	@Override
	public String getTranslationDescription() {
		return getModId() + ".abilities." + getUnlocalizedName() + (getDataManager().get(NEXT_ALIEN) ? ".next" : ".previous") + ".desc";
	}

	@Override
	public boolean action() {
		for (AbilityOmnitrix ab : Ability.getAbilitiesFromClass(Ability.getAbilities(entity), AbilityOmnitrix.class)) {
			ab.setSelectedAlien(ab.getSelectedAlien() + (this.getDataManager().get(NEXT_ALIEN) ? 1 : -1));
		}
		return true;
	}
}
