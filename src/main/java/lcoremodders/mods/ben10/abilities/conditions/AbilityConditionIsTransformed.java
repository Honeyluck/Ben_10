package lcoremodders.mods.ben10.abilities.conditions;

import lcoremodders.mods.ben10.abilities.AbilityOmnitrix;
import lucraft.mods.lucraftcore.superpowers.abilities.predicates.AbilityCondition;
import net.minecraft.util.text.TextComponentTranslation;

public class AbilityConditionIsTransformed extends AbilityCondition {

	public AbilityConditionIsTransformed(AbilityOmnitrix parentAb) {
		super((a) -> (parentAb.getDataManager().get(AbilityOmnitrix.IS_TRANSFORMED)), new TextComponentTranslation("lcoreben10.ability.condition.is_transformed"));
	}
}
