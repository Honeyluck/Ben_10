package lcoremodders.mods.ben10.abilities.data;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import lucraft.mods.lucraftcore.superpowers.abilities.data.AbilityData;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.util.JsonUtils;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.util.Constants;

import java.util.HashMap;

public class AbilityDataAlienList extends AbilityData<AbilityDataAlienList.AlienList> {

	public AbilityDataAlienList(String key) {
		super(key);
	}

	@Override
	public AlienList parseValue(JsonObject jsonObject, AlienList defaultValue) {
		if (jsonObject.has(this.jsonKey)) {
			AlienList alienList = new AlienList();

			JsonArray jsonAlienArray = JsonUtils.getJsonArray(jsonObject, this.jsonKey);
			for (int i = 0; i < jsonAlienArray.size(); i++) {
				if (jsonAlienArray.get(i) instanceof JsonObject) {
					String jsonAlienLoc = jsonAlienArray.get(i).getAsString();
					ResourceLocation alienLoc = new ResourceLocation(jsonAlienLoc);
					alienList.addAlien(i, alienLoc);
				}
			}

			return alienList;
		}
		return defaultValue;
	}

	@Override
	public void writeToNBT(NBTTagCompound nbt, AlienList value) {
		NBTTagList alienListTag = new NBTTagList();

		for (int key : value.aliens.keySet()) {
			NBTTagCompound alienTag = new NBTTagCompound();

			alienTag.setInteger("id", key);
			alienTag.setString("alienLoc", value.aliens.get(key).toString());

			alienListTag.appendTag(alienTag);
		}
		nbt.setTag(this.key, alienListTag);
	}

	@Override
	public AlienList readFromNBT(NBTTagCompound nbt, AlienList defaultValue) {
		if (nbt.hasKey(this.key)) {
			NBTTagList alienTagList = nbt.getTagList(this.key, Constants.NBT.TAG_COMPOUND);
			AlienList alienList = new AlienList();

			for (int i = 0; i < alienTagList.tagCount(); i++) {
				NBTTagCompound alienTagCompound = alienTagList.getCompoundTagAt(i);
				int id = alienTagCompound.getInteger("id");
				ResourceLocation alienLoc = new ResourceLocation(alienTagCompound.getString("alienLoc"));

				alienList.aliens.put(id, alienLoc);
			}

			return alienList;
		}
		return defaultValue;
	}

	@Override
	public String getDisplay(AlienList value) {
		String jsonString = "";
		for (int key : value.aliens.keySet()) {
			if (key == 0)
				jsonString = "\"" + value.aliens.get(key).toString() + "\"";
			else
				jsonString += ", \"" + value.aliens.get(key).toString() + "\"";

			System.out.println(key + ", " + value.aliens.get(key).toString());
		}
		return "[ " + jsonString + " ]";
	}

	public static class AlienList {
		private HashMap<Integer, ResourceLocation> aliens;

		public AlienList() {
			aliens = new HashMap<>();
		}

		public AlienList(String... alienLocs) {
			this();

			for (int i = 0; i < alienLocs.length; i++) {
				addAlien(i, new ResourceLocation(alienLocs[i]));
			}
		}

		public AlienList addAlien(int id, ResourceLocation alienLoc) {
			aliens.put(id, alienLoc);
			return this;
		}

		public HashMap<Integer, ResourceLocation> getAliens() {
			return aliens;
		}
	}
}