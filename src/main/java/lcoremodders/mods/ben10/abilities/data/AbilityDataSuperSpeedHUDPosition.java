package lcoremodders.mods.ben10.abilities.data;

import com.google.gson.JsonObject;
import lcoremodders.mods.ben10.abilities.AbilitySuperSpeed;
import lucraft.mods.lucraftcore.superpowers.abilities.data.AbilityData;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.JsonUtils;

import java.util.Arrays;

public class AbilityDataSuperSpeedHUDPosition extends AbilityData<AbilitySuperSpeed.Position> {

	public AbilityDataSuperSpeedHUDPosition(String key) {
		super(key);
	}

	@Override
	public AbilitySuperSpeed.Position parseValue(JsonObject jsonObject, AbilitySuperSpeed.Position defaultValue) {
		String value = JsonUtils.getString(jsonObject, this.jsonKey, defaultValue.getName());
		if (AbilitySuperSpeed.Position.isInside(value)) {
			return AbilitySuperSpeed.Position.getValue(value);
		} else {
			String[] valid_values = new String[AbilitySuperSpeed.Position.values().length];
			int id = 0;
			for (AbilitySuperSpeed.Position pos : AbilitySuperSpeed.Position.values()) {
				valid_values[id] = pos.getName();
				id++;
			}
			try {
				throw new Exception("Value '%VAL' is invalid. Valid values are: %VAL_ARR".replace("%VAL", value).replace("%VAL_ARR", Arrays.toString(valid_values)));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	@Override
	public void writeToNBT(NBTTagCompound nbt, AbilitySuperSpeed.Position value) {
		nbt.setString(this.key, value.getName());
	}

	@Override
	public AbilitySuperSpeed.Position readFromNBT(NBTTagCompound nbt, AbilitySuperSpeed.Position defaultValue) {
		if (!nbt.hasKey(this.key))
			return defaultValue;
		return AbilitySuperSpeed.Position.getValue(nbt.getString(this.key));
	}
}