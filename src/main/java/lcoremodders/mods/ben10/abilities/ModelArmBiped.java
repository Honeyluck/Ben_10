package lcoremodders.mods.ben10.abilities;

import net.minecraft.client.model.ModelBiped;
import net.minecraft.client.model.ModelRenderer;

public class ModelArmBiped extends ModelBiped {

	public ModelArmBiped() {
		super(0, 0, 64, 64);

		this.bipedLeftArm = new ModelRenderer(this, 32, 48);
		this.bipedLeftArm.addBox(-1.0F, -2.0F, -2.0F, 4, 12, 4, 0);
		this.bipedLeftArm.setRotationPoint(5.0F, 2.0F, 0.0F);
	}
}