package lcoremodders.mods.ben10.abilities;

import lcoremodders.mods.ben10.Ben10;
import lcoremodders.mods.ben10.abilities.data.AbilityDataAlienList;
import lcoremodders.mods.ben10.aliens.Alien;
import lcoremodders.mods.ben10.aliens.AlienHandler;
import lcoremodders.mods.ben10.aliens.Ben10Aliens;
import lcoremodders.mods.ben10.client.model.ModelOmnitrix;
import lcoremodders.mods.ben10.items.ItemOGOmnitrix;
import lcoremodders.mods.ben10.util.Ben10RenderHelper;
import lucraft.mods.lucraftcore.extendedinventory.InventoryExtendedInventory;
import lucraft.mods.lucraftcore.extendedinventory.capabilities.CapabilityExtendedInventory;
import lucraft.mods.lucraftcore.superpowers.abilities.Ability;
import lucraft.mods.lucraftcore.superpowers.abilities.AbilityToggle;
import lucraft.mods.lucraftcore.superpowers.abilities.data.AbilityData;
import lucraft.mods.lucraftcore.superpowers.abilities.data.AbilityDataBoolean;
import lucraft.mods.lucraftcore.superpowers.abilities.data.AbilityDataColor;
import lucraft.mods.lucraftcore.superpowers.abilities.data.AbilityDataInteger;
import lucraft.mods.lucraftcore.superpowers.abilities.supplier.EnumSync;
import lucraft.mods.lucraftcore.superpowers.render.RenderSuperpowerLayerEvent;
import lucraft.mods.lucraftcore.util.helper.LCRenderHelper;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.model.ModelBiped;
import net.minecraft.client.model.ModelPlayer;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.MobEffects;
import net.minecraft.util.EnumHandSide;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.event.RenderHandEvent;
import net.minecraftforge.client.event.RenderPlayerEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.glu.Sphere;

import java.awt.*;
import java.util.Random;

public class AbilityOmnitrix extends AbilityToggle {

	public static final AbilityData<AbilityDataAlienList.AlienList> ALIEN_LIST = new AbilityDataAlienList("aliens").setSyncType(EnumSync.SELF).disableSaving().enableSetting("aliens", "Sets the aliens available to access for this ability.");
	public static final AbilityData<Boolean> IS_TRANSFORMED = new AbilityDataBoolean("is_transformed").setSyncType(EnumSync.SELF);
	public static final AbilityData<Boolean> IS_SELECTING_ALIEN = new AbilityDataBoolean("is_selecting_alien").setSyncType(EnumSync.SELF);
	private static final AbilityData<Integer> SELECTED_ALIEN = new AbilityDataInteger("selected_alien").setSyncType(EnumSync.SELF);
	public static final AbilityData<Boolean> IS_RECHARGING = new AbilityDataBoolean("is_recharging").setSyncType(EnumSync.SELF);

	// Transformation Data
	private static final AbilityData<Integer> MAX_TRANSFORMATION_TIMER = new AbilityDataInteger("max_transformation_timer").setSyncType(EnumSync.SELF).disableSaving().enableSetting("max_transformation_timer", "Sets how long the transformation animation lasts. (Seconds * 20)");
	private static final AbilityData<Integer> TRANSFORMATION_TIMER = new AbilityDataInteger("transformation_timer").setSyncType(EnumSync.SELF);
	public static final AbilityData<Boolean> IS_TRANSFORMING = new AbilityDataBoolean("is_transforming");

	// STYLE
	private static final AbilityData<Color> TRANSFORMATION_COLOR = new AbilityDataColor("transformation_color").disableSaving().enableSetting("transformation_color", "Sets the color for transformation.");

	public AbilityOmnitrix(EntityLivingBase entity) {
		super(entity);
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void drawIcon(Minecraft mc, Gui gui, int x, int y) {
		Ben10RenderHelper.drawIcon(mc, gui, x, y, 0, 5);
	}

	@Override
	public void registerData() {
		super.registerData();
		this.dataManager.register(ALIEN_LIST, new AbilityDataAlienList.AlienList(
				"lcoreben10:four_arms", "lcoreben10:heatblast", "lcoreben10:xlr8"
		));
		this.dataManager.register(IS_TRANSFORMED, false);
		this.dataManager.register(IS_SELECTING_ALIEN, false);
		this.dataManager.register(SELECTED_ALIEN, 0);
		this.dataManager.register(IS_RECHARGING, false);

		this.dataManager.register(MAX_TRANSFORMATION_TIMER, 4*20);
		this.dataManager.register(TRANSFORMATION_TIMER, 0);
		this.dataManager.register(IS_TRANSFORMING, false);

		this.dataManager.register(TRANSFORMATION_COLOR, new Color(0, 153, 15));
	}

	@Override
	public boolean action() {
		boolean b = super.action();

		if (!isEnabled()) {
			getDataManager().set(IS_RECHARGING, false);
			for (AbilitySelectionMode ab : Ability.getAbilitiesFromClass(Ability.getAbilities(entity), AbilitySelectionMode.class)) {
				if (ab.isEnabled()) ab.action();
			}
			for (AbilityAlienTransform ab : Ability.getAbilitiesFromClass(Ability.getAbilities(entity), AbilityAlienTransform.class)) {
				if (ab.isEnabled()) ab.action();
			}

			for (AbilitySuperJump abilitySuperJump : Ability.getAbilitiesFromClass(Ability.getAbilities(entity), AbilitySuperJump.class)) {
				if (abilitySuperJump.getCharge() > 0) {
					if (!isEnabled()) {
						abilitySuperJump.setHasLanded(false);
						abilitySuperJump.setCharge(0);
					}

					for (AbilityAlienTransform abilityAlienTransform : Ability.getAbilitiesFromClass(Ability.getAbilities(entity), AbilityAlienTransform.class)) {
						if (!abilityAlienTransform.isEnabled()) {
							abilitySuperJump.setHasLanded(false);
							abilitySuperJump.setCharge(0);
						}
					}
				}
			}
		}
		return b;
	}

	@Override
	public void updateTick() {
		for (AbilitySuperJump abilitySuperJump : Ability.getAbilitiesFromClass(Ability.getAbilities(entity), AbilitySuperJump.class)) {
			if (abilitySuperJump.getCharge() > 0) {
				for (AbilityAlienTransform abilityAlienTransform : Ability.getAbilitiesFromClass(Ability.getAbilities(entity), AbilityAlienTransform.class)) {
					if (!abilityAlienTransform.isEnabled()) {
						abilitySuperJump.setHasLanded(false);
						abilitySuperJump.setCharge(0);
					}
				}
			}
		}

		if (getDataManager().get(IS_TRANSFORMING)) {
			if (getTransformationTimer() >= getMaxTransformationTimer()) {
				getDataManager().set(IS_TRANSFORMING, false);
				setTransformationTimer(0);
			} else {
				setTransformationTimer(getTransformationTimer() + 1);
			}
			System.out.println("Seconds:" + getTransformationTimer()/20);
		}
	}

	public void setSelectedAlien(int selectedAlien) {
		if (selectedAlien >= AlienHandler.getAliensSize(entity)) {
			selectedAlien = 0;
		} else if (selectedAlien < 0) {
			selectedAlien = AlienHandler.getAliensSize(entity) - 1;
		}
		this.getDataManager().set(SELECTED_ALIEN, selectedAlien);
	}

	public int getSelectedAlien() {
		return getDataManager().get(SELECTED_ALIEN);
	}

	public int getMaxTransformationTimer() {
		return getDataManager().get(MAX_TRANSFORMATION_TIMER);
	}
	public int getTransformationTimer() {
		return getDataManager().get(TRANSFORMATION_TIMER);
	}

	public void setTransformationTimer(int timer) {
		this.getDataManager().set(TRANSFORMATION_TIMER, timer);
	}

	public Alien getAlien(int id) {
		ResourceLocation alienLoc = getDataManager().get(ALIEN_LIST).getAliens().get(id);
		for (Alien alien : AlienHandler.ALIEN_REGISTRY.values()) {
			if (alien.getRegistryName().toString().equals(alienLoc.toString())) {
				return alien;
			}
		}
		return null;
	}

	@Mod.EventBusSubscriber(modid = Ben10.MODID, value = Side.CLIENT)
	public static class Renderer {
		private static ModelOmnitrix modelOmnitrix = new ModelOmnitrix();

		public static ModelBiped getAlienModel(Alien alien) {
			if (Ben10Aliens.Client.ALIEN_MODELS.containsKey(alien.getClass())) {
				return Ben10Aliens.Client.ALIEN_MODELS.get(alien.getClass());
			}
			return null;
		}

		@SubscribeEvent
		public static void onRenderSuperpowerLayerEvent(RenderSuperpowerLayerEvent e) {
			EntityPlayer player = e.getPlayer();
			if (!player.isPotionActive(MobEffects.INVISIBILITY)) {
				ModelPlayer playerModel = e.getRenderPlayer().getMainModel();

				if (AlienHandler.getAbilityOmnitrix(player) != null && AlienHandler.getAbilityOmnitrix(player).isEnabled() && getAlienModel(AlienHandler.getAlien(player)) != null) {
					AbilityOmnitrix abilityOmnitrix = AlienHandler.getAbilityOmnitrix(player);

					if (abilityOmnitrix.getDataManager().get(AbilityOmnitrix.IS_TRANSFORMING)) {
						renderTransformationLight(e.getPartialTicks(), AlienHandler.getAbilityOmnitrix(player));
					} else {
						if (AlienHandler.isTransformed(player)) {
							Alien alien = AlienHandler.getAlien(player);

							ModelBiped alienModel = getAlienModel(alien);
							alienModel.swingProgress = player.swingProgress;
							alienModel.isSneak = player.isSneaking();
							alienModel.isRiding = player.isRiding();

							playerModel.bipedHead.isHidden = true;
							playerModel.bipedBody.isHidden = true;
							playerModel.bipedRightArm.isHidden = true;
							playerModel.bipedLeftArm.isHidden = true;
							playerModel.bipedRightLeg.isHidden = true;
							playerModel.bipedLeftLeg.isHidden = true;
							playerModel.bipedHeadwear.isHidden = true;
							playerModel.bipedBodyWear.isHidden = true;
							playerModel.bipedRightArmwear.isHidden = true;
							playerModel.bipedLeftArmwear.isHidden = true;
							playerModel.bipedRightLegwear.isHidden = true;
							playerModel.bipedLeftLegwear.isHidden = true;

							Minecraft.getMinecraft().getTextureManager().bindTexture(alien.getAlienTexture(false));
							alienModel.render(player, e.getLimbSwing(), e.getLimbSwingAmount(), e.getAgeInTicks(), e.getNetHeadYaw(), e.getHeadPitch(), e.getScale());

							if (alien.hasLights()) {
								Minecraft.getMinecraft().getTextureManager().bindTexture(alien.getAlienTexture(true));
								GlStateManager.pushMatrix();
								GlStateManager.enableBlend();
								GlStateManager.disableLighting();
								GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
								GlStateManager.color(1, 1, 1, 1);
								LCRenderHelper.setLightmapTextureCoords(240, 240);
								alienModel.render(player, e.getLimbSwing(), e.getLimbSwingAmount(), e.getAgeInTicks(), e.getNetHeadYaw(), e.getHeadPitch(), e.getScale());
								LCRenderHelper.restoreLightmapTextureCoords();
								GlStateManager.enableLighting();
								GlStateManager.disableBlend();
								GlStateManager.popMatrix();
							}

							if (alien.getRenderer() != null)
								alien.getRenderer().render(alienModel, Minecraft.getMinecraft(), player, e.getLimbSwing(), e.getLimbSwingAmount(), e.getPartialTicks(), e.getAgeInTicks(), e.getNetHeadYaw(), e.getHeadPitch(), e.getScale());
						}
					}
				}

				if (!AlienHandler.isTransformed(player)) {
					playerModel.bipedHead.isHidden = false;
					playerModel.bipedBody.isHidden = false;
					playerModel.bipedRightArm.isHidden = false;
					playerModel.bipedLeftArm.isHidden = false;
					playerModel.bipedRightLeg.isHidden = false;
					playerModel.bipedLeftLeg.isHidden = false;
					playerModel.bipedHeadwear.isHidden = false;
					playerModel.bipedBodyWear.isHidden = false;
					playerModel.bipedRightArmwear.isHidden = false;
					playerModel.bipedLeftArmwear.isHidden = false;
					playerModel.bipedRightLegwear.isHidden = false;
					playerModel.bipedLeftLegwear.isHidden = false;
				}
			}
		}

		@SubscribeEvent
		public static void onRenderPlayerPreEvent(RenderPlayerEvent.Pre e) {
			EntityPlayer player = e.getEntityPlayer();
			if (!player.isPotionActive(MobEffects.INVISIBILITY)) {
				if (AlienHandler.getAbilityOmnitrix(player) != null && AlienHandler.getAbilityOmnitrix(player).isEnabled() && getAlienModel(AlienHandler.getAlien(player)) != null) {
					AbilityOmnitrix abilityOmnitrix = AlienHandler.getAbilityOmnitrix(player);

					if (abilityOmnitrix.getDataManager().get(AbilityOmnitrix.IS_TRANSFORMING)) {
						/*e.setCanceled(true);
						GlStateManager.pushMatrix();
						GlStateManager.translate(0, 1, 0);
						renderTransformationLight(e.getPartialRenderTick(), AlienHandler.getAbilityOmnitrix(player));
						GlStateManager.translate(0, -1, 0);
						GlStateManager.popMatrix();*/
					}
				}
			}
		}

		@SubscribeEvent
		public static void onRenderPlayerEvent(RenderPlayerEvent.Post e) {
			EntityPlayer player = e.getEntityPlayer();
			if (!player.isPotionActive(MobEffects.INVISIBILITY)) {
				if (AlienHandler.getAbilityOmnitrix(player) != null && AlienHandler.getAbilityOmnitrix(player).isEnabled() && getAlienModel(AlienHandler.getAlien(player)) != null) {
					AbilityOmnitrix abilityOmnitrix = AlienHandler.getAbilityOmnitrix(player);

					if (abilityOmnitrix.getDataManager().get(AbilityOmnitrix.IS_TRANSFORMING)) {
						/*GlStateManager.pushMatrix();
						GlStateManager.translate(0, 1, 0);
						renderTransformationLight(e.getPartialRenderTick(), AlienHandler.getAbilityOmnitrix(player));
						GlStateManager.translate(0, -1, 0);
						GlStateManager.popMatrix();*/
					}
				}
			}
		}

		private static ModelBiped biped = new ModelArmBiped();

		@SubscribeEvent(receiveCanceled = true)
		public static void onRenderHandEvent(RenderHandEvent e) {
			Minecraft mc = Minecraft.getMinecraft();
			EntityPlayerSP player = mc.player;

			if (AlienHandler.isTransformed(player)) {
				e.setCanceled(true);
			} else {
				e.setCanceled(false);
			}

			if ((!player.getCapability(CapabilityExtendedInventory.EXTENDED_INVENTORY_CAP, null).getInventory().getStackInSlot(InventoryExtendedInventory.SLOT_WRIST).isEmpty() && player.getCapability(CapabilityExtendedInventory.EXTENDED_INVENTORY_CAP, null).getInventory().getStackInSlot(InventoryExtendedInventory.SLOT_WRIST).getItem() instanceof ItemOGOmnitrix)) {
				for (AbilityOmnitrix ab : Ability.getAbilitiesFromClass(Ability.getAbilities(player), AbilityOmnitrix.class)) {
					if (!ab.getDataManager().get(AbilityOmnitrix.IS_TRANSFORMED) && ab.getDataManager().get(AbilityOmnitrix.IS_SELECTING_ALIEN)) {
						if (mc.gameSettings.thirdPersonView == 0 && !mc.gameSettings.hideGUI) {
							renderOmnitrix(mc, player);
						}
						if (!e.isCanceled()) {
							if (player.getPrimaryHand() == EnumHandSide.LEFT || (player.getPrimaryHand() == EnumHandSide.RIGHT && !player.getHeldItemOffhand().isEmpty())) {
								e.setCanceled(true);
							} else {
								e.setCanceled(false);
							}
						}
					}
				}
			}
		}

		private static void renderOmnitrix(Minecraft mc, EntityPlayerSP player) {
			GlStateManager.pushMatrix();
			float minY = -0.8f;
			float maxY = -0.7f;
			float ay = minY + player.getPitchYaw().x / 20;
			if (ay > maxY) {
				ay = maxY;
			}
			if (ay < minY) {
				ay = minY;
			}
			float minR = -10;
			float maxR = 30;
			float ar = minR + player.getPitchYaw().x;

			if (ar > maxR) {
				ar = maxR;
			}
			if (ar < minR) {
				ar = minR;
			}

				GlStateManager.pushMatrix();
				GlStateManager.translate(-0.2F, ay, -0.4F);
				GlStateManager.rotate(180, 0, 1, 0);
				GlStateManager.rotate(ar, 0, 0, 1);
				GlStateManager.rotate(45, 1, 0, 0);
				mc.renderEngine.bindTexture(player.getLocationSkin());
				biped.bipedLeftArm.setTextureOffset(32, 48);
				biped.bipedLeftArm.render(0.0625f);

				biped.bipedLeftArm.postRender(0.0625f);
				GlStateManager.scale(1, 1f, 1);
				GlStateManager.translate(1/16f, 5.5/16F, 0);
				GlStateManager.rotate(90.0F, 1.0F, 0.0F, 0.0F);
				GlStateManager.rotate(0.0F, 0.0F, 0.0F, 1.0F);

				modelOmnitrix.render(player, 0.0625F);
				GlStateManager.popMatrix();

			GlStateManager.popMatrix();
		}

		// 150 for transformation tick
		private static final Sphere TRANSFORMATION_BALL = new Sphere();

		private static void renderTransformationLight(float partialTicks, AbilityOmnitrix abilityOmnitrix) {
			Color color = abilityOmnitrix.getDataManager().get(AbilityOmnitrix.TRANSFORMATION_COLOR);
			GlStateManager.pushMatrix();
			Tessellator tessellator = Tessellator.getInstance();
			BufferBuilder bufferbuilder = tessellator.getBuffer();
			RenderHelper.disableStandardItemLighting();
			float f = ((float)abilityOmnitrix.getTransformationTimer() + partialTicks) / 200.0F;
			float f1 = 0.0F;

			if (f > 0.8F)
			{
				f1 = (f - 0.8F) / 0.2F;
			}

			Random random = new Random(432L);
			GlStateManager.disableTexture2D();
			GlStateManager.shadeModel(7425);
			GlStateManager.enableBlend();
			GlStateManager.blendFunc(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE);
			GlStateManager.disableAlpha();
			GlStateManager.enableCull();
			GlStateManager.depthMask(false);
			GlStateManager.pushMatrix();
			GlStateManager.translate(0.0F, 0F, -0F);

			for (int i = 0; (float)i < (f + f * f) / 2.0F * 60.0F; ++i)
			{
				GlStateManager.rotate(random.nextFloat() * 360.0F, 1.0F, 0.0F, 0.0F);
				GlStateManager.rotate(random.nextFloat() * 360.0F, 0.0F, 1.0F, 0.0F);
				GlStateManager.rotate(random.nextFloat() * 360.0F, 0.0F, 0.0F, 1.0F);
				GlStateManager.rotate(random.nextFloat() * 360.0F, 1.0F, 0.0F, 0.0F);
				GlStateManager.rotate(random.nextFloat() * 360.0F, 0.0F, 1.0F, 0.0F);
				GlStateManager.rotate(random.nextFloat() * 360.0F + f * 90.0F, 0.0F, 0.0F, 1.0F);
				float f2 = random.nextFloat() * 20.0F + 5.0F + f1 * 10.0F;
				float f3 = random.nextFloat() * 2.0F + 1.0F + f1 * 2.0F;
				bufferbuilder.begin(6, DefaultVertexFormats.POSITION_COLOR);
				bufferbuilder.pos(0.0D, 0.0D, 0.0D).color(color.getRed(), color.getGreen(), color.getBlue(), (int)(255.0F * (1.0F - f1))).endVertex();
				bufferbuilder.pos(-0.866D * (double)f3, (double)f2, (double)(-0.5F * f3)).color(color.getRed(), color.getGreen(), color.getBlue(), 0).endVertex();
				bufferbuilder.pos(0.866D * (double)f3, (double)f2, (double)(-0.5F * f3)).color(color.getRed(), color.getGreen(), color.getBlue(), 0).endVertex();
				bufferbuilder.pos(0.0D, (double)f2, (double)(1.0F * f3)).color(color.getRed(), color.getGreen(), color.getBlue(), 0).endVertex();
				bufferbuilder.pos(-0.866D * (double)f3, (double)f2, (double)(-0.5F * f3)).color(color.getRed(), color.getGreen(), color.getBlue(), 0).endVertex();
				tessellator.draw();
			}

			GlStateManager.popMatrix();
			GlStateManager.depthMask(true);
			GlStateManager.disableCull();
			GlStateManager.disableBlend();
			GlStateManager.shadeModel(7424);
			GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
			GlStateManager.enableTexture2D();
			GlStateManager.enableAlpha();
			RenderHelper.enableStandardItemLighting();

			GlStateManager.popMatrix();

			// Sphere
			GlStateManager.pushMatrix();
			GlStateManager.shadeModel(7425);
			GlStateManager.enableBlend();
			GlStateManager.blendFunc(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE);
			GlStateManager.disableAlpha();
			//GlStateManager.enableCull();
			GlStateManager.disableDepth();

			GlStateManager.translate(0, 0.5, 0);
			GlStateManager.color(color.getRed()/255F, color.getGreen()/255F, color.getBlue()/255F, 1f/*(255.0F * (1.0F - f1))*/);
			TRANSFORMATION_BALL.draw((abilityOmnitrix.getTransformationTimer()/20f)/2f, 20, 20);
			GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);

			GlStateManager.enableDepth();
			//GlStateManager.disableCull();
			GlStateManager.disableBlend();
			GlStateManager.shadeModel(7424);
			GlStateManager.enableAlpha();
			GlStateManager.popMatrix();
		}
	}
}