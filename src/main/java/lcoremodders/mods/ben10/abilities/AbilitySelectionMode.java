package lcoremodders.mods.ben10.abilities;

import lcoremodders.mods.ben10.util.Ben10RenderHelper;
import lucraft.mods.lucraftcore.superpowers.abilities.Ability;
import lucraft.mods.lucraftcore.superpowers.abilities.AbilityToggle;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Gui;
import net.minecraft.entity.EntityLivingBase;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class AbilitySelectionMode extends AbilityToggle {

	public AbilitySelectionMode(EntityLivingBase entity) {
		super(entity);
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void drawIcon(Minecraft mc, Gui gui, int x, int y) {
		Ben10RenderHelper.drawIcon(mc, gui, x, y, 0, 4);
	}

	@Override
	public boolean action() {
		boolean b = super.action();

		for (AbilityOmnitrix ab : Ability.getAbilitiesFromClass(Ability.getAbilities(entity), AbilityOmnitrix.class)) {
			ab.getDataManager().set(AbilityOmnitrix.IS_SELECTING_ALIEN, isEnabled());
		}
		return b;
	}

	@Override
	public void updateTick() {}
}