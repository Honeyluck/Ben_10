package lcoremodders.mods.ben10.abilities;

import lcoremodders.mods.ben10.util.Ben10RenderHelper;
import lucraft.mods.lucraftcore.superpowers.abilities.AbilityHeld;
import lucraft.mods.lucraftcore.superpowers.abilities.data.AbilityData;
import lucraft.mods.lucraftcore.superpowers.abilities.data.AbilityDataBoolean;
import lucraft.mods.lucraftcore.superpowers.abilities.data.AbilityDataInteger;
import lucraft.mods.lucraftcore.superpowers.abilities.supplier.EnumSync;
import lucraft.mods.lucraftcore.util.abilitybar.AbilityBarHandler;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Gui;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.network.play.server.SPacketEntityVelocity;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.Vec3d;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class AbilitySuperJump extends AbilityHeld {

	public static final AbilityData<Integer> CHARGE = new AbilityDataInteger("charge").disableSaving().setSyncType(EnumSync.SELF);
	public static final AbilityData<Integer> MAX_CHARGE = new AbilityDataInteger("max_charge").disableSaving().setSyncType(EnumSync.SELF).enableSetting("max_charge", "Charge affects how high you will jump.");
	public static final AbilityData<Boolean> HAS_LANDED = new AbilityDataBoolean("has_landed").setSyncType(EnumSync.SELF);

	public AbilitySuperJump(EntityLivingBase entity) {
		super(entity);
	}

	@Override
	public void registerData() {
		super.registerData();

		this.dataManager.register(CHARGE, 0);
		this.dataManager.register(MAX_CHARGE, 3);
		this.dataManager.register(HAS_LANDED, false);
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void drawIcon(Minecraft mc, Gui gui, int x, int y) {
		Ben10RenderHelper.drawIcon(mc, gui, x, y, 0, 3);
	}

	@Override
	public void drawAdditionalInfo(Minecraft mc, Gui gui, int x, int y) {
		super.drawAdditionalInfo(mc, gui, x, y);

		mc.renderEngine.bindTexture(AbilityBarHandler.Renderer.HUD_TEX);
		float cooldown = (float) getCharge() / (float) getMaxCharge();
		gui.drawTexturedModalRect(x - 1, y, 0, 33, (int) (cooldown * 18), 3);
	}

	@Override
	public void updateTick() {
		if (isEnabled()) {
			setCharge(getCharge() + 1);
			setHasLanded(false);
		}
	}

	public void jump() {
		float f = getCharge() / 10F;
		Vec3d vec = getEntity().getLookVec();
		getEntity().addVelocity(vec.x * f * 4F, f, vec.z * f * 4F);
		getEntity().onGround = false;

		/*if (getEntity() instanceof EntityPlayer)
			((EntityPlayer) getEntity()).capabilities.allowFlying = true;*/

		if (getEntity() instanceof EntityPlayerMP)
			((EntityPlayerMP) getEntity()).connection.sendPacket(new SPacketEntityVelocity(getEntity()));
	}

	public int getCharge() {
		return MathHelper.clamp(getDataManager().get(CHARGE), 0, getMaxCharge());
	}

	public void setCharge(int charge) {
		getDataManager().set(CHARGE, MathHelper.clamp(charge, 0, getMaxCharge()));
	}

	public int getMaxCharge() {
		// NOTE: One second per 20 TICKS, so X seconds * 20 ticks. Time units is in ticks
		return getDataManager().get(MAX_CHARGE) * 20;
	}

	public boolean hasLanded() {
		return getDataManager().get(HAS_LANDED);
	}

	public void setHasLanded(boolean hasLanded) {
		getDataManager().set(HAS_LANDED, hasLanded);
	}
}