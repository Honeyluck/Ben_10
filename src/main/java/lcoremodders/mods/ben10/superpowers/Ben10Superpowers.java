package lcoremodders.mods.ben10.superpowers;

import lucraft.mods.lucraftcore.superpowers.Superpower;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

@Mod.EventBusSubscriber
public class Ben10Superpowers {

	public static Superpower omnitrix;

	@SubscribeEvent
	public static void init(RegistryEvent.Register<Superpower> e) {
		e.getRegistry().register(omnitrix = new SuperpowerOmnitrix());
	}
}