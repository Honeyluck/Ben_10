package lcoremodders.mods.ben10.client.model;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelBox;
import net.minecraft.client.model.ModelRenderer;

/**
 * OmnitrixUpdated - Undefined
 * Created using Tabula 7.0.1
 */
public class ModelOmnitrixFacePlate extends ModelBase {

    public ModelRenderer Parent;
    public ModelRenderer Selector;

    public ModelOmnitrixFacePlate() {
        this.textureWidth = 12;
        this.textureHeight = 3;

        Selector = new ModelRenderer(this);
	    Selector.setRotationPoint(0.0F, -3.0F, -0.5F);
	    Selector.cubeList.add(new ModelBox(Selector, 0, 0, -1.5F, 0.1F, -1.5F, 3, 0, 3, 0.0F, false));

        this.Parent = new ModelRenderer(this, 0, 0);
        this.Parent.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.Parent.addBox(0.0F, 0.0F, 0.0F, 0, 0, 0, 0.0F);

        this.Parent.addChild(this.Selector);
    }

    public void render(float scale) {
        this.Parent.render(scale);
    }

    /**
     * This is a helper function from Tabula to set the rotation of model parts
     */
    public void setRotateAngle(ModelRenderer modelRenderer, float x, float y, float z) {
        modelRenderer.rotateAngleX = x;
        modelRenderer.rotateAngleY = y;
        modelRenderer.rotateAngleZ = z;
    }
}
