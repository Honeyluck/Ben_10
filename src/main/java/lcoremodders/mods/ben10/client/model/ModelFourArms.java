package lcoremodders.mods.ben10.client.model;

import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;
import net.minecraft.util.EnumHandSide;

/**
 * fourarms_ben - BenTennyson
 * Created using Tabula 7.0.0
 */
public class ModelFourArms extends ModelAlien {

	public ModelRenderer leftleg;
	public ModelRenderer rightleg;
	public ModelRenderer body;
	public ModelRenderer head;
	public ModelRenderer leftarm;
	public ModelRenderer rightarm;
	public ModelRenderer leftarm2;
	public ModelRenderer rightarm2;
	public ModelRenderer toes;
	public ModelRenderer toes_1;
	public ModelRenderer foot;
	public ModelRenderer heel;
	public ModelRenderer heel_1;
	public ModelRenderer toes_2;
	public ModelRenderer toes_3;
	public ModelRenderer foot_1;
	public ModelRenderer heel_2;
	public ModelRenderer heel_3;
	public ModelRenderer body2;
	public ModelRenderer chin;
	public ModelRenderer eye1;
	public ModelRenderer eye2;
	public ModelRenderer eye3;
	public ModelRenderer eye4;
	public ModelRenderer leftarmb;
	public ModelRenderer leftarmc;
	public ModelRenderer finger;
	public ModelRenderer finger_1;
	public ModelRenderer finger_2;
	public ModelRenderer finger_3;
	public ModelRenderer rightarmb;
	public ModelRenderer rightarmc;
	public ModelRenderer finger_4;
	public ModelRenderer finger_5;
	public ModelRenderer finger_6;
	public ModelRenderer finger_7;
	public ModelRenderer leftarm2b;
	public ModelRenderer leftarm2c;
	public ModelRenderer finger_8;
	public ModelRenderer finger_9;
	public ModelRenderer finger_10;
	public ModelRenderer finger_11;
	public ModelRenderer rightarm2b;
	public ModelRenderer rightarm2c;
	public ModelRenderer finger_12;
	public ModelRenderer finger_13;
	public ModelRenderer finger_14;
	public ModelRenderer finger_15;

	public ModelFourArms() {
		super(64, 64);

		this.finger_5 = new ModelRenderer(this, 57, 45);
		this.finger_5.setRotationPoint(0.0F, 0.0F, 0.0F);
		this.finger_5.addBox(-7.4F, 12.05F, -0.5F, 2, 1, 1, 0.0F);
		this.setRotateAngle(finger_5, 0.0F, 0.0F, -0.7853981633974483F);
		this.rightarmb = new ModelRenderer(this, 0, 54);
		this.rightarmb.mirror = true;
		this.rightarmb.setRotationPoint(0.0F, 0.0F, 0.0F);
		this.rightarmb.addBox(-4.7F, 2.4F, -2.0F, 4, 6, 4, 0.0F);
		this.setRotateAngle(rightarmb, 0.0F, 0.0F, -0.39269908169872414F);
		this.finger_14 = new ModelRenderer(this, 57, 45);
		this.finger_14.setRotationPoint(0.0F, 0.0F, 0.0F);
		this.finger_14.addBox(-3.9F, 12.2F, 0.75F, 2, 1, 1, 0.0F);
		this.setRotateAngle(finger_14, 0.0F, 0.0F, -0.39269908169872414F);
		this.heel_1 = new ModelRenderer(this, 54, 54);
		this.heel_1.setRotationPoint(0.0F, 0.0F, 0.0F);
		this.heel_1.addBox(-1.51F, 11.01F, 0.51F, 3, 1, 1, 0.0F);
		this.eye2 = new ModelRenderer(this, 56, 51);
		this.eye2.setRotationPoint(0.0F, 0.0F, 0.0F);
		this.eye2.addBox(3.0F, -3.8F, -4.01F, 2, 1, 1, 0.0F);
		this.setRotateAngle(eye2, 0.0F, 0.0F, -0.39269908169872414F);
		this.leftarmc = new ModelRenderer(this, 17, 52);
		this.leftarmc.setRotationPoint(0.0F, 0.0F, 0.0F);
		this.leftarmc.addBox(2.75F, 5.75F, -2.5F, 5, 7, 5, 0.0F);
		this.setRotateAngle(leftarmc, 0.0F, 0.0F, 0.7853981633974483F);
		this.toes_3 = new ModelRenderer(this, 54, 58);
		this.toes_3.setRotationPoint(0.0F, 0.0F, 0.0F);
		this.toes_3.addBox(-1.5F, 11.0F, -5.5F, 1, 1, 4, 0.0F);
		this.finger_12 = new ModelRenderer(this, 57, 45);
		this.finger_12.setRotationPoint(0.0F, 0.0F, 0.0F);
		this.finger_12.addBox(-3.9F, 12.2F, -1.75F, 2, 1, 1, 0.0F);
		this.setRotateAngle(finger_12, 0.0F, 0.0F, -0.39269908169872414F);
		this.eye4 = new ModelRenderer(this, 56, 51);
		this.eye4.setRotationPoint(0.0F, 0.0F, 0.0F);
		this.eye4.addBox(-5.1F, -3.8F, -4.01F, 2, 1, 1, 0.0F);
		this.setRotateAngle(eye4, 0.0F, 0.0F, 0.39269908169872414F);
		this.finger_7 = new ModelRenderer(this, 57, 45);
		this.finger_7.setRotationPoint(0.0F, 0.0F, 0.0F);
		this.finger_7.addBox(-5.25F, 11.25F, -2.8F, 2, 1, 1, 0.0F);
		this.setRotateAngle(finger_7, 0.0F, 0.0F, -0.7853981633974483F);
		this.finger_8 = new ModelRenderer(this, 57, 45);
		this.finger_8.setRotationPoint(0.0F, 0.0F, 0.0F);
		this.finger_8.addBox(1.9F, 12.2F, -1.75F, 2, 1, 1, 0.0F);
		this.setRotateAngle(finger_8, 0.0F, 0.0F, 0.39269908169872414F);
		this.leftarmb = new ModelRenderer(this, 0, 54);
		this.leftarmb.setRotationPoint(0.0F, 0.0F, 0.0F);
		this.leftarmb.addBox(0.7F, 2.4F, -2.0F, 4, 6, 4, 0.0F);
		this.setRotateAngle(leftarmb, 0.0F, 0.0F, 0.39269908169872414F);
		this.eye1 = new ModelRenderer(this, 56, 51);
		this.eye1.setRotationPoint(0.0F, 0.0F, 0.0F);
		this.eye1.addBox(3.6F, -5.3F, -4.01F, 2, 1, 1, 0.0F);
		this.setRotateAngle(eye1, 0.0F, 0.0F, -0.39269908169872414F);
		this.finger_1 = new ModelRenderer(this, 57, 45);
		this.finger_1.setRotationPoint(0.0F, 0.0F, 0.0F);
		this.finger_1.addBox(5.6F, 12.05F, -0.5F, 2, 1, 1, 0.0F);
		this.setRotateAngle(finger_1, 0.0F, 0.0F, 0.7853981633974483F);
		this.heel_3 = new ModelRenderer(this, 54, 54);
		this.heel_3.setRotationPoint(0.0F, 0.0F, 0.0F);
		this.heel_3.addBox(-1.51F, 11.01F, 0.51F, 3, 1, 1, 0.0F);
		this.toes_2 = new ModelRenderer(this, 54, 58);
		this.toes_2.setRotationPoint(0.0F, 0.0F, 0.0F);
		this.toes_2.addBox(0.5F, 11.0F, -5.5F, 1, 1, 4, 0.0F);
		this.foot_1 = new ModelRenderer(this, 54, 54);
		this.foot_1.setRotationPoint(0.0F, 0.0F, 0.0F);
		this.foot_1.addBox(-1.5F, 11.0F, -2.5F, 3, 1, 1, 0.0F);
		this.finger_9 = new ModelRenderer(this, 57, 45);
		this.finger_9.setRotationPoint(0.0F, 0.0F, 0.0F);
		this.finger_9.addBox(1.9F, 12.2F, -0.5F, 2, 1, 1, 0.0F);
		this.setRotateAngle(finger_9, 0.0F, 0.0F, 0.39269908169872414F);
		this.finger_2 = new ModelRenderer(this, 57, 45);
		this.finger_2.setRotationPoint(0.0F, 0.0F, 0.0F);
		this.finger_2.addBox(5.6F, 12.05F, 1.0F, 2, 1, 1, 0.0F);
		this.setRotateAngle(finger_2, 0.0F, 0.0F, 0.7853981633974483F);
		this.rightarm2 = new ModelRenderer(this, 33, 4);
		this.rightarm2.mirror = true;
		this.rightarm2.setRotationPoint(-4.5F, 0.0F, 0.0F);
		this.rightarm2.addBox(-2.2F, -0.2F, -1.99F, 4, 5, 4, 0.0F);
		this.setRotateAngle(rightarm2, 0.0F, 0.0F, 0.39269908169872414F);
		this.heel = new ModelRenderer(this, 54, 54);
		this.heel.setRotationPoint(0.0F, 0.0F, 0.0F);
		this.heel.addBox(-1.49F, 11.01F, 0.51F, 3, 1, 1, 0.0F);
		this.finger_3 = new ModelRenderer(this, 57, 45);
		this.finger_3.setRotationPoint(0.0F, 0.0F, 0.0F);
		this.finger_3.addBox(3.25F, 11.45F, -2.8F, 2, 1, 1, 0.0F);
		this.setRotateAngle(finger_3, 0.0F, 0.0F, 0.7853981633974483F);
		this.finger_6 = new ModelRenderer(this, 57, 45);
		this.finger_6.setRotationPoint(0.0F, 0.0F, 0.0F);
		this.finger_6.addBox(-7.4F, 12.05F, 1.0F, 2, 1, 1, 0.0F);
		this.setRotateAngle(finger_6, 0.0F, 0.0F, -0.7853981633974483F);
		this.toes_1 = new ModelRenderer(this, 54, 58);
		this.toes_1.setRotationPoint(0.0F, 0.0F, 0.0F);
		this.toes_1.addBox(-1.5F, 11.0F, -5.5F, 1, 1, 4, 0.0F);
		this.heel_2 = new ModelRenderer(this, 54, 54);
		this.heel_2.setRotationPoint(0.0F, 0.0F, 0.0F);
		this.heel_2.addBox(-1.49F, 11.01F, 0.51F, 3, 1, 1, 0.0F);
		this.leftarm2b = new ModelRenderer(this, 33, 14);
		this.leftarm2b.setRotationPoint(0.0F, 0.0F, 0.0F);
		this.leftarm2b.addBox(0.5F, 3.7F, -1.5F, 3, 4, 3, 0.0F);
		this.setRotateAngle(leftarm2b, 0.0F, 0.0F, 0.39269908169872414F);
		this.body = new ModelRenderer(this, 0, 16);
		this.body.setRotationPoint(0.0F, 0.0F, 0.0F);
		this.body.addBox(-4.5F, 0.0F, -2.0F, 9, 12, 4, 0.0F);
		this.rightarm2c = new ModelRenderer(this, 33, 22);
		this.rightarm2c.mirror = true;
		this.rightarm2c.setRotationPoint(0.0F, 0.0F, 0.0F);
		this.rightarm2c.addBox(-4.0F, 7.0F, -2.0F, 4, 6, 4, 0.0F);
		this.setRotateAngle(rightarm2c, 0.0F, 0.0F, -0.39269908169872414F);
		this.finger_13 = new ModelRenderer(this, 57, 45);
		this.finger_13.setRotationPoint(0.0F, 0.0F, 0.0F);
		this.finger_13.addBox(-3.9F, 12.2F, -0.5F, 2, 1, 1, 0.0F);
		this.setRotateAngle(finger_13, 0.0F, 0.0F, -0.39269908169872414F);
		this.finger_15 = new ModelRenderer(this, 57, 45);
		this.finger_15.setRotationPoint(0.0F, 0.0F, 0.0F);
		this.finger_15.addBox(-2.45F, 11.7F, -2.2F, 2, 1, 1, 0.0F);
		this.setRotateAngle(finger_15, 0.0F, 0.0F, -0.39269908169872414F);
		this.toes = new ModelRenderer(this, 54, 58);
		this.toes.setRotationPoint(0.0F, 0.0F, 0.0F);
		this.toes.addBox(0.5F, 11.0F, -5.5F, 1, 1, 4, 0.0F);
		this.leftleg = new ModelRenderer(this, 52, 0);
		this.leftleg.setRotationPoint(3.0F, 12.0F, 0.0F);
		this.leftleg.addBox(-1.5F, 0.0F, -1.5F, 3, 12, 3, 0.0F);
		this.finger = new ModelRenderer(this, 57, 45);
		this.finger.setRotationPoint(0.0F, 0.0F, 0.0F);
		this.finger.addBox(5.6F, 11.95F, -2.0F, 2, 1, 1, 0.0F);
		this.setRotateAngle(finger, 0.0F, 0.0F, 0.7853981633974483F);
		this.rightleg = new ModelRenderer(this, 52, 15);
		this.rightleg.setRotationPoint(-3.0F, 12.0F, 0.0F);
		this.rightleg.addBox(-1.5F, 0.0F, -1.5F, 3, 12, 3, 0.0F);
		this.eye3 = new ModelRenderer(this, 56, 51);
		this.eye3.setRotationPoint(0.0F, 0.0F, 0.0F);
		this.eye3.addBox(-5.5F, -5.3F, -4.01F, 2, 1, 1, 0.0F);
		this.setRotateAngle(eye3, 0.0F, 0.0F, 0.39269908169872414F);
		this.chin = new ModelRenderer(this, 24, 0);
		this.chin.setRotationPoint(0.0F, 0.0F, 0.0F);
		this.chin.addBox(-4.0F, -2.01F, -4.5F, 8, 2, 1, 0.0F);
		this.leftarm2 = new ModelRenderer(this, 33, 4);
		this.leftarm2.setRotationPoint(4.5F, 0.0F, 0.0F);
		this.leftarm2.addBox(-1.8F, -0.2F, -1.99F, 4, 5, 4, 0.0F);
		this.setRotateAngle(leftarm2, 0.0F, 0.0F, -0.39269908169872414F);
		this.finger_4 = new ModelRenderer(this, 57, 45);
		this.finger_4.setRotationPoint(0.0F, 0.0F, 0.0F);
		this.finger_4.addBox(-7.4F, 11.95F, -2.0F, 2, 1, 1, 0.0F);
		this.setRotateAngle(finger_4, 0.0F, 0.0F, -0.7853981633974483F);
		this.leftarm2c = new ModelRenderer(this, 33, 22);
		this.leftarm2c.setRotationPoint(0.0F, 0.0F, 0.0F);
		this.leftarm2c.addBox(0.0F, 7.0F, -2.0F, 4, 6, 4, 0.0F);
		this.setRotateAngle(leftarm2c, 0.0F, 0.0F, 0.39269908169872414F);
		this.body2 = new ModelRenderer(this, 0, 32);
		this.body2.setRotationPoint(0.0F, 0.0F, 0.0F);
		this.body2.addBox(-6.5F, -6.0F, -2.5F, 13, 6, 5, 0.0F);
		this.foot = new ModelRenderer(this, 54, 54);
		this.foot.setRotationPoint(0.0F, 0.0F, 0.0F);
		this.foot.addBox(-1.5F, 11.0F, -2.5F, 3, 1, 1, 0.0F);
		this.rightarmc = new ModelRenderer(this, 17, 52);
		this.rightarmc.mirror = true;
		this.rightarmc.setRotationPoint(0.0F, 0.0F, 0.0F);
		this.rightarmc.addBox(-7.75F, 5.75F, -2.5F, 5, 7, 5, 0.0F);
		this.setRotateAngle(rightarmc, 0.0F, 0.0F, -0.7853981633974483F);
		this.leftarm = new ModelRenderer(this, 0, 43);
		this.leftarm.setRotationPoint(6.0F, -3.0F, 0.0F);
		this.leftarm.addBox(-1.4F, -1.8F, -2.5F, 5, 6, 5, 0.0F);
		this.setRotateAngle(leftarm, 0.0F, 0.0F, -0.7853981633974483F);
		this.rightarm2b = new ModelRenderer(this, 33, 14);
		this.rightarm2b.mirror = true;
		this.rightarm2b.setRotationPoint(0.0F, 0.0F, 0.0F);
		this.rightarm2b.addBox(-3.5F, 3.7F, -1.5F, 3, 4, 3, 0.0F);
		this.setRotateAngle(rightarm2b, 0.0F, 0.0F, -0.39269908169872414F);
		this.head = new ModelRenderer(this, 0, 0);
		this.head.setRotationPoint(0.0F, -6.0F, 0.0F);
		this.head.addBox(-4.0F, -8.0F, -4.0F, 8, 8, 8, 0.0F);
		this.finger_10 = new ModelRenderer(this, 57, 45);
		this.finger_10.setRotationPoint(0.0F, 0.0F, 0.0F);
		this.finger_10.addBox(1.9F, 12.2F, 0.75F, 2, 1, 1, 0.0F);
		this.setRotateAngle(finger_10, 0.0F, 0.0F, 0.39269908169872414F);
		this.finger_11 = new ModelRenderer(this, 57, 45);
		this.finger_11.setRotationPoint(0.0F, 0.0F, 0.0F);
		this.finger_11.addBox(0.45F, 11.7F, -2.2F, 2, 1, 1, 0.0F);
		this.setRotateAngle(finger_11, 0.0F, 0.0F, 0.39269908169872414F);
		this.rightarm = new ModelRenderer(this, 0, 43);
		this.rightarm.mirror = true;
		this.rightarm.setRotationPoint(-6.0F, -3.0F, 0.0F);
		this.rightarm.addBox(-3.6F, -1.8F, -2.5F, 5, 6, 5, 0.0F);
		this.setRotateAngle(rightarm, 0.0F, 0.0F, 0.7853981633974483F);
		this.rightarm.addChild(this.finger_5);
		this.rightarm.addChild(this.rightarmb);
		this.rightarm2.addChild(this.finger_14);
		this.leftleg.addChild(this.heel_1);
		this.head.addChild(this.eye2);
		this.leftarm.addChild(this.leftarmc);
		this.rightleg.addChild(this.toes_3);
		this.rightarm2.addChild(this.finger_12);
		this.head.addChild(this.eye4);
		this.rightarm.addChild(this.finger_7);
		this.leftarm2.addChild(this.finger_8);
		this.leftarm.addChild(this.leftarmb);
		this.head.addChild(this.eye1);
		this.leftarm.addChild(this.finger_1);
		this.rightleg.addChild(this.heel_3);
		this.rightleg.addChild(this.toes_2);
		this.rightleg.addChild(this.foot_1);
		this.leftarm2.addChild(this.finger_9);
		this.leftarm.addChild(this.finger_2);
		this.leftleg.addChild(this.heel);
		this.leftarm.addChild(this.finger_3);
		this.rightarm.addChild(this.finger_6);
		this.leftleg.addChild(this.toes_1);
		this.rightleg.addChild(this.heel_2);
		this.leftarm2.addChild(this.leftarm2b);
		this.rightarm2.addChild(this.rightarm2c);
		this.rightarm2.addChild(this.finger_13);
		this.rightarm2.addChild(this.finger_15);
		this.leftleg.addChild(this.toes);
		this.leftarm.addChild(this.finger);
		this.head.addChild(this.eye3);
		this.head.addChild(this.chin);
		this.rightarm.addChild(this.finger_4);
		this.leftarm2.addChild(this.leftarm2c);
		this.body.addChild(this.body2);
		this.leftleg.addChild(this.foot);
		this.rightarm.addChild(this.rightarmc);
		this.rightarm2.addChild(this.rightarm2b);
		this.leftarm2.addChild(this.finger_10);
		this.leftarm2.addChild(this.finger_11);
	}

	@Override
	public void render(Entity entityIn, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch, float scale) {
		super.render(entityIn, limbSwing, limbSwingAmount, ageInTicks, netHeadYaw, headPitch, scale);

		this.rightarm2.render(scale);
		this.body.render(scale);
		this.leftleg.render(scale);
		this.rightleg.render(scale);
		this.leftarm2.render(scale);
		this.leftarm.render(scale);
		this.head.render(scale);
		this.rightarm.render(scale);
	}

	@Override
	public void setRotationAngles(float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch, float scaleFactor, Entity entityIn) {
		super.setRotationAngles(limbSwing, limbSwingAmount, ageInTicks, netHeadYaw, headPitch, scaleFactor, entityIn);

		this.leftleg.rotateAngleX = this.bipedLeftLeg.rotateAngleX;
		this.rightleg.rotateAngleX = this.bipedRightLeg.rotateAngleX;

		this.body.rotateAngleX = this.bipedBody.rotateAngleX;
		this.body.rotateAngleY = this.bipedBody.rotateAngleY;
		this.body.rotateAngleZ = this.bipedBody.rotateAngleZ;

		this.head.rotateAngleX = this.bipedHead.rotateAngleX;
		this.head.rotateAngleY = this.bipedHead.rotateAngleY;
		this.head.rotateAngleZ = this.bipedHead.rotateAngleZ;

		this.leftarm.rotateAngleX = this.bipedLeftArm.rotateAngleX;
		this.leftarm2.rotateAngleX = this.bipedLeftArm.rotateAngleX;
		this.rightarm.rotateAngleX = this.bipedRightArm.rotateAngleX;
		this.rightarm2.rotateAngleX = this.bipedRightArm.rotateAngleX;
	}

	@Override
	protected ModelRenderer getArmForSide(EnumHandSide side) {
		return side == EnumHandSide.LEFT ? this.leftarm2 : this.rightarm2;
	}

	/**
	 * This is a helper function from Tabula to set the rotation of model parts
	 */
	public void setRotateAngle(ModelRenderer modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}
}
