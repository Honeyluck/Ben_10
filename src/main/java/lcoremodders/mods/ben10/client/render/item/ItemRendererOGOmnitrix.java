package lcoremodders.mods.ben10.client.render.item;


import lcoremodders.mods.ben10.aliens.AlienHandler;
import lcoremodders.mods.ben10.client.model.ModelOmnitrix;
import lcoremodders.mods.ben10.superpowers.Ben10Superpowers;
import lucraft.mods.lucraftcore.extendedinventory.render.ExtendedInventoryItemRendererRegistry;
import lucraft.mods.lucraftcore.superpowers.SuperpowerHandler;
import net.minecraft.client.model.ModelBiped;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.entity.RenderLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumHandSide;

public class ItemRendererOGOmnitrix implements ExtendedInventoryItemRendererRegistry.IItemExtendedInventoryRenderer {

	private static ModelOmnitrix modelOmnitrix = new ModelOmnitrix();

	@Override
	public void render(EntityPlayer player, RenderLivingBase<?> render, ItemStack stack, float limbSwing, float limbSwingAmount, float partialTicks, float ageInTicks, float netHeadYaw, float headPitch, float scale, boolean isHead) {
		if (isHead)
			return;

		if (stack != null) {
			if (!SuperpowerHandler.hasSuperpower(player, Ben10Superpowers.omnitrix) || (SuperpowerHandler.hasSuperpower(player, Ben10Superpowers.omnitrix) && !AlienHandler.isTransformed(player))) {
				GlStateManager.pushMatrix();
				if (render.getMainModel().isChild) {
					GlStateManager.translate(0.0F, 0.625F, 0.0F);
					GlStateManager.rotate(-20.0F, -1.0F, 0.0F, 0.0F);
					GlStateManager.scale(0.5F, 0.5F, 0.5F);
				}
				if (player.isSneaking()) {
					GlStateManager.translate(0.0F, 0.2F, 0.0F);
				}
				((ModelBiped) render.getMainModel()).postRenderArm(0.0625F, EnumHandSide.LEFT);
				GlStateManager.scale(1, 1f, 1);
				GlStateManager.translate(1/16f, 5.5/16F, 0);
				GlStateManager.rotate(90.0F, 1.0F, 0.0F, 0.0F);
				GlStateManager.rotate(90.0F, 0.0F, 0.0F, 1.0F);


				modelOmnitrix.render(player, 0.0625F);

				GlStateManager.popMatrix();
			}
		}
	}
}